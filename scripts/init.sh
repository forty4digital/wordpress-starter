#!/usr/bin/env bash

WORK_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)";
PROJ_DIR="$(dirname "$WORK_DIR")"
PROJECT_NAME="$( echo "$PROJ_DIR" | rev | cut -d '/' -f1 | rev )";

source "${0%/*}/./default.conf";

echo "Generating WordPress site."
echo "Answer the following questions or press enter to use the default settings."

echo -n "Project Name (${PROJECT_NAME}): "
read PROJECT_NAME_INPUT
PROJECT_NAME="${PROJECT_NAME_INPUT:="${PROJECT_NAME}"}"

echo -n "Git Repo (${REPO_URL}): "
read REPO_URL_INPUT
REPO_URL="${REPO_URL_INPUT:="${REPO_URL}"}"

echo -n "AWS RDS Endpoint (${RDS_ENDPOINT}): "
read RDS_ENDPOINT_INPUT
RDS_ENDPOINT="${RDS_ENDPOINT_INPUT:="${RDS_ENDPOINT}"}"

echo -n "AWS EC2 Public DNS (${EC2_DNS}): "
read EC2_DNS_INPUT
EC2_DNS="${EC2_DNS_INPUT:="${EC2_DNS}"}"


echo -n "Site Title (${PROJECT_NAME}): "
read SITE_TITLE_INPUT
SITE_TITLE="${SITE_TITLE_INPUT:="${PROJECT_NAME}"}"

echo -n "Local URL (${PROJECT_NAME}.lan): "
read LOCAL_URL_INPUT
LOCAL_URL="${LOCAL_URL_INPUT:="${PROJECT_NAME}.lan"}"

echo -n "Production URL (${PROJECT_NAME}.com): "
read PROD_URL_INPUT
PROD_URL="${PROD_URL_INPUT:="${PROJECT_NAME}.com"}"

echo -n "WordPress Port (${WP_PORT}): "
read WP_PORT_INPUT
WP_PORT="${WP_PORT_INPUT:="${WP_PORT}"}"

echo -n "MySQL Port (${MYSQL_PORT}): "
read MYSQL_PORT_INPUT
MYSQL_PORT="${MYSQL_PORT_INPUT:="${MYSQL_PORT}"}"

echo -n "MySQL Database (${MYSQL_DB}): "
read MYSQL_DB_INPUT
MYSQL_DB="${MYSQL_DB_INPUT:="${MYSQL_DB}"}"

echo -n "MySQL User (${MYSQL_USER}): "
read MYSQL_USER_INPUT
MYSQL_USER="${MYSQL_USER_INPUT:="${MYSQL_USER}"}"

echo -n "MySQL User Password (${MYSQL_PW}): "
read MYSQL_PW_INPUT
MYSQL_PW="${MYSQL_PW_INPUT:="${MYSQL_PW}"}"

echo -n "MySQL Root Password (${MYSQL_ROOT_PW}): "
read MYSQL_ROOT_PW_INPUT
MYSQL_ROOT_PW="${MYSQL_ROOT_PW_INPUT:="${MYSQL_ROOT_PW}"}"

echo "${blue}${bold}------------------------------------------------------------------------------------------"
echo " Project Settings:"
echo "------------------------------------------------------------------------------------------"
echo "   Working Directory:        ${WORK_DIR}"
echo "   Project Directory:        ${PROJ_DIR}"
echo "   Project Name:             ${PROJECT_NAME}"
echo "   Git Repo:                 ${REPO_URL}"
echo "   AWS RDS Endpoint:         ${RDS_ENDPOINT}"
echo "   AWS EC2 Public DNS:       ${EC2_DNS}"
echo "   Site Title:               ${SITE_TITLE}"
echo "   Local URL:                ${LOCAL_URL}"
echo "   Prod URL:                 ${PROD_URL}"
echo "   Database Name:            ${MYSQL_DB}"
echo "   MySQL User:               ${MYSQL_USER}"
echo "   MySQL User Pass:          ${MYSQL_PW}"
echo "   MySQL Root Pass:          ${MYSQL_ROOT_PW}"
echo "   MySQL Host Port:          ${MYSQL_PORT}"
echo "   WordPress Host Port:      ${WP_PORT}"
echo "   WordPress Admin User:     ${WP_USER}"
echo "   WordPress Admin Password: ${WP_PW}"
echo "------------------------------------------------------------------------------------------"

cat > "${PROJ_DIR}/project.conf" << EOF1
#! /bin/bash

WORK_DIR=${WORK_DIR}
PROJ_DIR=${PROJ_DIR}
PROJECT_NAME=${PROJECT_NAME}
REPO_URL=${REPO_URL}
RDS_ENDPOINT=${RDS_ENDPOINT}
EC2_DNS=${EC2_DNS}
SITE_TITLE=${SITE_TITLE}
LOCAL_URL=${LOCAL_URL}
PROD_URL=${PROD_URL}
MYSQL_DB=${MYSQL_DB}
MYSQL_USER=${MYSQL_USER}
MYSQL_PW=${MYSQL_PW}
MYSQL_ROOT_PW=${MYSQL_ROOT_PW}
MYSQL_PORT=${MYSQL_PORT}
WP_PORT=${WP_PORT}
WP_USER=${WP_USER}
WP_PW=${WP_PW}

EOF1

echo "${green}SUCCESS: Project Configuration Created";
echo "${reset}";
source "${0%/*}/./generate_docker_compose.sh";
source "${0%/*}/./generate_database_file.sh";
source "${0%/*}/./start.sh";
source "${0%/*}/./load_db.sh";
source "${0%/*}/./test_url.sh";

exit 0