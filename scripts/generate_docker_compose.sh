#!/usr/bin/env bash

if [ -e ${0%/*}/../docker-compose.yml ]; then
  echo "docker-compose.yml file exists. Do you want to overwrite it?"
  read -p "[y/n]? " choice
case "$choice" in
  y|Y )
  rm -rf ${0%/*}/../docker-compose.yml
  ;;
  n|N )
  clear
  echo "Goodbye!" && exit 0
  ;;
esac
fi

sed "s#MYSQLROOTPASSWORD#${MYSQL_ROOT_PW}#g" "${0%/*}/../docker-compose.yml.tmpl" > ./docker-compose.yml
sed -i -e "s#MYSQLUSER#${MYSQL_USER}#g" ./docker-compose.yml
sed -i -e "s#MYSQLPASSWORD#${MYSQL_PW}#g" ./docker-compose.yml
sed -i -e "s#DATABASENAME#${MYSQL_DB}#g" ./docker-compose.yml
sed -i -e "s#PROJECTNAME#${PROJECT_NAME}#g" ./docker-compose.yml
sed -i -e "s#DBPORT#${MYSQL_PORT}#g" ./docker-compose.yml
sed -i -e "s#WPPORT#${WP_PORT}#g" ./docker-compose.yml
sed -i -e "s#LOCALURL#${LOCAL_URL}#g" ./docker-compose.yml
rm ./docker-compose.yml-e

echo "${green}${bold}SUCCESS: docker-compose.yml File Created";
echo "${reset}";