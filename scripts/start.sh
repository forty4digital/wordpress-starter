#!/usr/bin/env bash

cd $PROJ_DIR
docker-compose --project-name="${PROJECT_NAME}" up -d;
sleep 15s;

echo "${green}${bold}SUCCESS: Service Containers Started";
echo "${reset}";