#!/usr/bin/env bash

cd $PROJ_DIR
docker-compose --project-name="${PROJECT_NAME}" down;
sleep 15s;

echo "${green}${bold}SUCCESS: Service Containers Removed";
echo "${reset}";