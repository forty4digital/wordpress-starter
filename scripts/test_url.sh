#!/usr/bin/env bash

echo "Testing URL."
bash -c 'while [[ "$( curl -s -o /dev/null -w ''%{http_code}'' '${LOCAL_URL}' )" != "200" ]]; do sleep 5; done'

echo "${green}${bold}SUCCESS: Local URL Responding";
echo "${reset}";

clear
echo "Project ${PROJECT_NAME} created! Visit your new WordPress site: ${LOCAL_URL}"
exit 0;