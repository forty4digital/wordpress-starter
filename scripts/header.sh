#!/usr/bin/env bash


blue=$(tput setaf 4)
bold=$(tput bold)
green=$(tput setaf 2)
red=$(tput setaf 1)
reset=$(tput sgr0)
yellow=$(tput setaf 3)

traperr() {
  echo "${red}ERROR: ${BASH_SOURCE[1]} at about ${BASH_LINENO[0]}${rerset}"
}

set -o errtrace
trap traperr ERR

clear
cat <<- EOF
${blue}${bold}
                 ______ __ __         __
.--.--.--.-----.|   __ \__|  |.-----.|  |_
|  |  |  |  _  ||    __/  |  ||  _  ||   _|
|________|   __||___|  |__|__||_____||____|
         |__|


${yellow}
---------------------------------------------------------------
 A CLI for initializing and managing WordPress deployments
 with Docker Compose.
---------------------------------------------------------------


EOF



if [ ! -e ${0%/*}/../project.conf ]; then
  echo "${reset} no project found, would you like to create one?"
  read -p "[y/n]? " choice
case "$choice" in
  y|Y )
  clear
  source "${0%/*}/./init.sh"
  ;;
  n|N )
  clear
  echo "Goodbye!" && exit 0
  ;;
esac
fi

source "${0%/*}/../project.conf"

Actions[0]="Start Containers"
Actions[1]="Stop Containers"
Actions[2]="Import Database"
Actions[3]="Export Database"
Actions[4]="Project Info"
Actions[5]="Reset Project"

PS3='Select an action: '

select opt in "${Actions[@]}";
do
  echo "You picked $opt"
  SELECTED=${opt}
  break;
done

case "$SELECTED" in
  "Start Containers")
    source "${0%/*}/./start.sh";
    ;;

  "Stop Containers")
    source "${0%/*}/./stop.sh";
    ;;

  "Import Database")
    echo "Importing"
    ;;

  "Export Database")
    echo "Exporting"
    ;;

  "Project Info")
    source "${0%/*}/../project.conf";
    echo "${blue}${bold}------------------------------------------------------------------------------------------"
    echo " Project Settings:"
    echo "------------------------------------------------------------------------------------------"
    echo "   Working Directory:        ${WORK_DIR}"
    echo "   Project Directory:        ${PROJ_DIR}"
    echo "   Project Name:             ${PROJECT_NAME}"
    echo "   Git Repo:                 ${REPO_URL}"
    echo "   AWS RDS Endpoint:         ${RDS_ENDPOINT}"
    echo "   AWS EC2 Public DNS:       ${EC2_DNS}"
    echo "   Site Title:               ${SITE_TITLE}"
    echo "   Local URL:                ${LOCAL_URL}"
    echo "   Prod URL:                 ${PROD_URL}"
    echo "   Database Name:            ${MYSQL_DB}"
    echo "   MySQL User:               ${MYSQL_USER}"
    echo "   MySQL User Pass:          ${MYSQL_PW}"
    echo "   MySQL Root Pass:          ${MYSQL_ROOT_PW}"
    echo "   MySQL Host Port:          ${MYSQL_PORT}"
    echo "   WordPress Host Port:      ${WP_PORT}"
    echo "   WordPress Admin User:     ${WP_USER}"
    echo "   WordPress Admin Password: ${WP_PW}"
    echo "------------------------------------------------------------------------------------------"
    ;;

  "Reset Project")
    source "${0%/*}/./reset.sh";
    ;;
esac