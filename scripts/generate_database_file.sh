#!/usr/bin/env bash

if [ -e ${0%/*}/../init_db.sql ]; then
  echo "init_db.sql file exists. Do you want to overwrite it?"
  read -p "[y/n]? " choice
case "$choice" in
  y|Y )
  rm -rf ${0%/*}/../init_db.sql
  ;;
  n|N )
  clear
  echo "Goodbye!" && exit 0
  ;;
esac
fi

sed "s#DATABASENAME#${MYSQL_DB}#g" "${0%/*}/../init_db.sql.tmpl" > init_db.sql
sed -i -e "s#SITEURL#${LOCAL_URL}#g" ./init_db.sql
sed -i -e "s#SITETITLE#${SITE_TITLE}#g" ./init_db.sql
rm ./init_db.sql-e

echo "${green}${bold}SUCCESS: init_db.sql File Created";
echo "${reset}";