#!/usr/bin/env bash

cd $PROJ_DIR
docker-compose --project-name="${PROJECT_NAME}" down;
sleep 15s;

docker volume rm "${PROJECT_NAME}_db_data";

rm ./project.conf ./init_db.sql ./docker-compose.yml ./wp-data/wp-admin ./wp-data/wp-includes
rm ./wp-data/wp-content/plugins

echo "${green}${bold}SUCCESS: Project Reset";
echo "${reset}";