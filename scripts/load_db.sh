#!/usr/bin/env bash

mysql -u root "-p${MYSQL_ROOT_PW}" --host="127.0.0.1" --port="${MYSQL_PORT}" --database="${MYSQL_DB}" < "./init_db.sql";
sleep 10s

echo "${green}${bold}SUCCESS: Database Loaded";
echo "${reset}";